db.users.insertOne(
{ "name" : "single",
   "accomodates": 2,
   "price": 1000,
   "description": "A simple room wwith all the basic necessities"
});

db.users.updateOne({
 "_id" : ObjectId("61360139fbdd34e84ddd4140")
 },{
     $set:{
         "rooms_available": 10,
         "isAvailable": false
     }
     
 });

 db.users.insertMany([
{
  "name": "queen",
  "accomodates": 4,
  "price": 4000,
  "description": "A room with a queen sized bed perfect for a simple getaway",
  "rooms_available": 15,
  "isAvailable": false
}]);

 db.users.findOne(
{ "name": "double" }
);

db.users.updateOne({
 "_id" : ObjectId("61360594fbdd34e84ddd4141")
 },{
     $set:{
         "rooms_available": 0
     }
     
 });

 db.users.deleteMany(
{ "rooms_available": 0 });